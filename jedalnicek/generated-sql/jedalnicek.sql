
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- user
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64) NOT NULL,
    `login` VARCHAR(64) NOT NULL,
    `email` VARCHAR(64) NOT NULL,
    `password` VARCHAR(32) NOT NULL,
    `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `facebook_id` INTEGER,
    `vip` TINYINT(1) DEFAULT 0,
    `fail_access` INTEGER DEFAULT 0,
    `first_access` TINYINT(1) DEFAULT 1,
    `last_access` DATETIME,
    `age` INTEGER,
    `height` FLOAT(4),
    `weight` FLOAT(4),
    `sport` FLOAT(4),
    `gender` VARCHAR(1) DEFAULT 'M',
    `bmr` FLOAT(4),
    `program` INTEGER DEFAULT 1,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- food
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `food`;

CREATE TABLE `food`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(64) NOT NULL,
    `amount` VARCHAR(12) NOT NULL,
    `energy_kj` FLOAT(4),
    `energy_kcal` FLOAT(4),
    `protein` FLOAT(4),
    `fat` FLOAT(4),
    `sugar` FLOAT(4),
    `category` VARCHAR(64),
    `tags` VARCHAR(64),
    `fluid` VARCHAR(12),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- plate
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `plate`;

CREATE TABLE `plate`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(32) NOT NULL,
    `date` DATE NOT NULL,
    `dish` VARCHAR(10) DEFAULT 'breakfast' NOT NULL,
    `user_id` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `plate_FI_1` (`user_id`),
    CONSTRAINT `plate_FK_1`
        FOREIGN KEY (`user_id`)
        REFERENCES `user` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- plate_food
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `plate_food`;

CREATE TABLE `plate_food`
(
    `plate_id` INTEGER NOT NULL,
    `food_id` INTEGER NOT NULL,
    `amount` FLOAT(4) DEFAULT 100,
    PRIMARY KEY (`plate_id`,`food_id`),
    INDEX `plate_food_FI_2` (`food_id`),
    CONSTRAINT `plate_food_FK_1`
        FOREIGN KEY (`plate_id`)
        REFERENCES `plate` (`id`),
    CONSTRAINT `plate_food_FK_2`
        FOREIGN KEY (`food_id`)
        REFERENCES `food` (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

-- ---------------------------------------------------------------------
-- food_image
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `food_image`;

CREATE TABLE `food_image`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `url` VARCHAR(64) NOT NULL,
    `tag` VARCHAR(32),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
