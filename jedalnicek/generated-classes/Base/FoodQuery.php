<?php

namespace Base;

use \Food as ChildFood;
use \FoodQuery as ChildFoodQuery;
use \Exception;
use \PDO;
use Map\FoodTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'food' table.
 *
 *
 *
 * @method     ChildFoodQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFoodQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildFoodQuery orderByAmount($order = Criteria::ASC) Order by the amount column
 * @method     ChildFoodQuery orderByEnergyKj($order = Criteria::ASC) Order by the energy_kj column
 * @method     ChildFoodQuery orderByEnergyKcal($order = Criteria::ASC) Order by the energy_kcal column
 * @method     ChildFoodQuery orderByProtein($order = Criteria::ASC) Order by the protein column
 * @method     ChildFoodQuery orderByFat($order = Criteria::ASC) Order by the fat column
 * @method     ChildFoodQuery orderBySugar($order = Criteria::ASC) Order by the sugar column
 * @method     ChildFoodQuery orderByCategory($order = Criteria::ASC) Order by the category column
 * @method     ChildFoodQuery orderByTags($order = Criteria::ASC) Order by the tags column
 * @method     ChildFoodQuery orderByFluid($order = Criteria::ASC) Order by the fluid column
 *
 * @method     ChildFoodQuery groupById() Group by the id column
 * @method     ChildFoodQuery groupByName() Group by the name column
 * @method     ChildFoodQuery groupByAmount() Group by the amount column
 * @method     ChildFoodQuery groupByEnergyKj() Group by the energy_kj column
 * @method     ChildFoodQuery groupByEnergyKcal() Group by the energy_kcal column
 * @method     ChildFoodQuery groupByProtein() Group by the protein column
 * @method     ChildFoodQuery groupByFat() Group by the fat column
 * @method     ChildFoodQuery groupBySugar() Group by the sugar column
 * @method     ChildFoodQuery groupByCategory() Group by the category column
 * @method     ChildFoodQuery groupByTags() Group by the tags column
 * @method     ChildFoodQuery groupByFluid() Group by the fluid column
 *
 * @method     ChildFoodQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFoodQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFoodQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFoodQuery leftJoinPlateFood($relationAlias = null) Adds a LEFT JOIN clause to the query using the PlateFood relation
 * @method     ChildFoodQuery rightJoinPlateFood($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PlateFood relation
 * @method     ChildFoodQuery innerJoinPlateFood($relationAlias = null) Adds a INNER JOIN clause to the query using the PlateFood relation
 *
 * @method     ChildFood findOne(ConnectionInterface $con = null) Return the first ChildFood matching the query
 * @method     ChildFood findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFood matching the query, or a new ChildFood object populated from the query conditions when no match is found
 *
 * @method     ChildFood findOneById(int $id) Return the first ChildFood filtered by the id column
 * @method     ChildFood findOneByName(string $name) Return the first ChildFood filtered by the name column
 * @method     ChildFood findOneByAmount(string $amount) Return the first ChildFood filtered by the amount column
 * @method     ChildFood findOneByEnergyKj(double $energy_kj) Return the first ChildFood filtered by the energy_kj column
 * @method     ChildFood findOneByEnergyKcal(double $energy_kcal) Return the first ChildFood filtered by the energy_kcal column
 * @method     ChildFood findOneByProtein(double $protein) Return the first ChildFood filtered by the protein column
 * @method     ChildFood findOneByFat(double $fat) Return the first ChildFood filtered by the fat column
 * @method     ChildFood findOneBySugar(double $sugar) Return the first ChildFood filtered by the sugar column
 * @method     ChildFood findOneByCategory(string $category) Return the first ChildFood filtered by the category column
 * @method     ChildFood findOneByTags(string $tags) Return the first ChildFood filtered by the tags column
 * @method     ChildFood findOneByFluid(string $fluid) Return the first ChildFood filtered by the fluid column
 *
 * @method     array findById(int $id) Return ChildFood objects filtered by the id column
 * @method     array findByName(string $name) Return ChildFood objects filtered by the name column
 * @method     array findByAmount(string $amount) Return ChildFood objects filtered by the amount column
 * @method     array findByEnergyKj(double $energy_kj) Return ChildFood objects filtered by the energy_kj column
 * @method     array findByEnergyKcal(double $energy_kcal) Return ChildFood objects filtered by the energy_kcal column
 * @method     array findByProtein(double $protein) Return ChildFood objects filtered by the protein column
 * @method     array findByFat(double $fat) Return ChildFood objects filtered by the fat column
 * @method     array findBySugar(double $sugar) Return ChildFood objects filtered by the sugar column
 * @method     array findByCategory(string $category) Return ChildFood objects filtered by the category column
 * @method     array findByTags(string $tags) Return ChildFood objects filtered by the tags column
 * @method     array findByFluid(string $fluid) Return ChildFood objects filtered by the fluid column
 *
 */
abstract class FoodQuery extends ModelCriteria
{

    /**
     * Initializes internal state of \Base\FoodQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'jedalnicek', $modelName = '\\Food', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFoodQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFoodQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof \FoodQuery) {
            return $criteria;
        }
        $query = new \FoodQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFood|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = FoodTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FoodTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return   ChildFood A model object, or null if the key is not found
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT ID, NAME, AMOUNT, ENERGY_KJ, ENERGY_KCAL, PROTEIN, FAT, SUGAR, CATEGORY, TAGS, FLUID FROM food WHERE ID = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            $obj = new ChildFood();
            $obj->hydrate($row);
            FoodTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFood|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FoodTableMap::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FoodTableMap::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FoodTableMap::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FoodTableMap::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoodTableMap::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoodTableMap::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the amount column
     *
     * Example usage:
     * <code>
     * $query->filterByAmount('fooValue');   // WHERE amount = 'fooValue'
     * $query->filterByAmount('%fooValue%'); // WHERE amount LIKE '%fooValue%'
     * </code>
     *
     * @param     string $amount The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByAmount($amount = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($amount)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $amount)) {
                $amount = str_replace('*', '%', $amount);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoodTableMap::AMOUNT, $amount, $comparison);
    }

    /**
     * Filter the query on the energy_kj column
     *
     * Example usage:
     * <code>
     * $query->filterByEnergyKj(1234); // WHERE energy_kj = 1234
     * $query->filterByEnergyKj(array(12, 34)); // WHERE energy_kj IN (12, 34)
     * $query->filterByEnergyKj(array('min' => 12)); // WHERE energy_kj > 12
     * </code>
     *
     * @param     mixed $energyKj The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByEnergyKj($energyKj = null, $comparison = null)
    {
        if (is_array($energyKj)) {
            $useMinMax = false;
            if (isset($energyKj['min'])) {
                $this->addUsingAlias(FoodTableMap::ENERGY_KJ, $energyKj['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($energyKj['max'])) {
                $this->addUsingAlias(FoodTableMap::ENERGY_KJ, $energyKj['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoodTableMap::ENERGY_KJ, $energyKj, $comparison);
    }

    /**
     * Filter the query on the energy_kcal column
     *
     * Example usage:
     * <code>
     * $query->filterByEnergyKcal(1234); // WHERE energy_kcal = 1234
     * $query->filterByEnergyKcal(array(12, 34)); // WHERE energy_kcal IN (12, 34)
     * $query->filterByEnergyKcal(array('min' => 12)); // WHERE energy_kcal > 12
     * </code>
     *
     * @param     mixed $energyKcal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByEnergyKcal($energyKcal = null, $comparison = null)
    {
        if (is_array($energyKcal)) {
            $useMinMax = false;
            if (isset($energyKcal['min'])) {
                $this->addUsingAlias(FoodTableMap::ENERGY_KCAL, $energyKcal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($energyKcal['max'])) {
                $this->addUsingAlias(FoodTableMap::ENERGY_KCAL, $energyKcal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoodTableMap::ENERGY_KCAL, $energyKcal, $comparison);
    }

    /**
     * Filter the query on the protein column
     *
     * Example usage:
     * <code>
     * $query->filterByProtein(1234); // WHERE protein = 1234
     * $query->filterByProtein(array(12, 34)); // WHERE protein IN (12, 34)
     * $query->filterByProtein(array('min' => 12)); // WHERE protein > 12
     * </code>
     *
     * @param     mixed $protein The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByProtein($protein = null, $comparison = null)
    {
        if (is_array($protein)) {
            $useMinMax = false;
            if (isset($protein['min'])) {
                $this->addUsingAlias(FoodTableMap::PROTEIN, $protein['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($protein['max'])) {
                $this->addUsingAlias(FoodTableMap::PROTEIN, $protein['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoodTableMap::PROTEIN, $protein, $comparison);
    }

    /**
     * Filter the query on the fat column
     *
     * Example usage:
     * <code>
     * $query->filterByFat(1234); // WHERE fat = 1234
     * $query->filterByFat(array(12, 34)); // WHERE fat IN (12, 34)
     * $query->filterByFat(array('min' => 12)); // WHERE fat > 12
     * </code>
     *
     * @param     mixed $fat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByFat($fat = null, $comparison = null)
    {
        if (is_array($fat)) {
            $useMinMax = false;
            if (isset($fat['min'])) {
                $this->addUsingAlias(FoodTableMap::FAT, $fat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fat['max'])) {
                $this->addUsingAlias(FoodTableMap::FAT, $fat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoodTableMap::FAT, $fat, $comparison);
    }

    /**
     * Filter the query on the sugar column
     *
     * Example usage:
     * <code>
     * $query->filterBySugar(1234); // WHERE sugar = 1234
     * $query->filterBySugar(array(12, 34)); // WHERE sugar IN (12, 34)
     * $query->filterBySugar(array('min' => 12)); // WHERE sugar > 12
     * </code>
     *
     * @param     mixed $sugar The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterBySugar($sugar = null, $comparison = null)
    {
        if (is_array($sugar)) {
            $useMinMax = false;
            if (isset($sugar['min'])) {
                $this->addUsingAlias(FoodTableMap::SUGAR, $sugar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sugar['max'])) {
                $this->addUsingAlias(FoodTableMap::SUGAR, $sugar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FoodTableMap::SUGAR, $sugar, $comparison);
    }

    /**
     * Filter the query on the category column
     *
     * Example usage:
     * <code>
     * $query->filterByCategory('fooValue');   // WHERE category = 'fooValue'
     * $query->filterByCategory('%fooValue%'); // WHERE category LIKE '%fooValue%'
     * </code>
     *
     * @param     string $category The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByCategory($category = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($category)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $category)) {
                $category = str_replace('*', '%', $category);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoodTableMap::CATEGORY, $category, $comparison);
    }

    /**
     * Filter the query on the tags column
     *
     * Example usage:
     * <code>
     * $query->filterByTags('fooValue');   // WHERE tags = 'fooValue'
     * $query->filterByTags('%fooValue%'); // WHERE tags LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tags The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByTags($tags = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tags)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tags)) {
                $tags = str_replace('*', '%', $tags);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoodTableMap::TAGS, $tags, $comparison);
    }

    /**
     * Filter the query on the fluid column
     *
     * Example usage:
     * <code>
     * $query->filterByFluid('fooValue');   // WHERE fluid = 'fooValue'
     * $query->filterByFluid('%fooValue%'); // WHERE fluid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fluid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByFluid($fluid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fluid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fluid)) {
                $fluid = str_replace('*', '%', $fluid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(FoodTableMap::FLUID, $fluid, $comparison);
    }

    /**
     * Filter the query by a related \PlateFood object
     *
     * @param \PlateFood|ObjectCollection $plateFood  the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByPlateFood($plateFood, $comparison = null)
    {
        if ($plateFood instanceof \PlateFood) {
            return $this
                ->addUsingAlias(FoodTableMap::ID, $plateFood->getFoodId(), $comparison);
        } elseif ($plateFood instanceof ObjectCollection) {
            return $this
                ->usePlateFoodQuery()
                ->filterByPrimaryKeys($plateFood->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPlateFood() only accepts arguments of type \PlateFood or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PlateFood relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function joinPlateFood($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PlateFood');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PlateFood');
        }

        return $this;
    }

    /**
     * Use the PlateFood relation PlateFood object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \PlateFoodQuery A secondary query class using the current class as primary query
     */
    public function usePlateFoodQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPlateFood($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PlateFood', '\PlateFoodQuery');
    }

    /**
     * Filter the query by a related Plate object
     * using the plate_food table as cross reference
     *
     * @param Plate $plate the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function filterByPlate($plate, $comparison = Criteria::EQUAL)
    {
        return $this
            ->usePlateFoodQuery()
            ->filterByPlate($plate, $comparison)
            ->endUse();
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFood $food Object to remove from the list of results
     *
     * @return ChildFoodQuery The current query, for fluid interface
     */
    public function prune($food = null)
    {
        if ($food) {
            $this->addUsingAlias(FoodTableMap::ID, $food->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the food table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FoodTableMap::DATABASE_NAME);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FoodTableMap::clearInstancePool();
            FoodTableMap::clearRelatedInstancePool();

            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $affectedRows;
    }

    /**
     * Performs a DELETE on the database, given a ChildFood or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ChildFood object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public function delete(ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FoodTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FoodTableMap::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();


        FoodTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FoodTableMap::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

} // FoodQuery
