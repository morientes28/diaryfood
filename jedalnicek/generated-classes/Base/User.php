<?php

namespace Base;

use \Plate as ChildPlate;
use \PlateQuery as ChildPlateQuery;
use \User as ChildUser;
use \UserQuery as ChildUserQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\UserTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

abstract class User implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\UserTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the login field.
     * @var        string
     */
    protected $login;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the created field.
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        string
     */
    protected $created;

    /**
     * The value for the facebook_id field.
     * @var        int
     */
    protected $facebook_id;

    /**
     * The value for the vip field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $vip;

    /**
     * The value for the fail_access field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $fail_access;

    /**
     * The value for the first_access field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $first_access;

    /**
     * The value for the last_access field.
     * @var        string
     */
    protected $last_access;

    /**
     * The value for the age field.
     * @var        int
     */
    protected $age;

    /**
     * The value for the height field.
     * @var        double
     */
    protected $height;

    /**
     * The value for the weight field.
     * @var        double
     */
    protected $weight;

    /**
     * The value for the sport field.
     * @var        double
     */
    protected $sport;

    /**
     * The value for the gender field.
     * Note: this column has a database default value of: 'M'
     * @var        string
     */
    protected $gender;

    /**
     * The value for the bmr field.
     * @var        double
     */
    protected $bmr;

    /**
     * The value for the program field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $program;

    /**
     * @var        ObjectCollection|ChildPlate[] Collection to store aggregation of ChildPlate objects.
     */
    protected $collPlates;
    protected $collPlatesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection
     */
    protected $platesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->vip = false;
        $this->fail_access = 0;
        $this->first_access = true;
        $this->gender = 'M';
        $this->program = 1;
    }

    /**
     * Initializes internal state of Base\User object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !empty($this->modifiedColumns);
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return in_array($col, $this->modifiedColumns);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return array_unique($this->modifiedColumns);
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (Boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (Boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            while (false !== ($offset = array_search($col, $this->modifiedColumns))) {
                array_splice($this->modifiedColumns, $offset, 1);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>User</code> instance.  If
     * <code>obj</code> is an instance of <code>User</code>, delegates to
     * <code>equals(User)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        $thisclazz = get_class($this);
        if (!is_object($obj) || !($obj instanceof $thisclazz)) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey()
            || null === $obj->getPrimaryKey())  {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        if (null !== $this->getPrimaryKey()) {
            return crc32(serialize($this->getPrimaryKey()));
        }

        return crc32(serialize(clone $this));
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return User The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     *
     * @return User The current object, for fluid interface
     */
    public function importFrom($parser, $data)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), TableMap::TYPE_PHPNAME);

        return $this;
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        return array_keys(get_object_vars($this));
    }

    /**
     * Get the [id] column value.
     *
     * @return   int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return   string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [login] column value.
     *
     * @return   string
     */
    public function getLogin()
    {

        return $this->login;
    }

    /**
     * Get the [email] column value.
     *
     * @return   string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [password] column value.
     *
     * @return   string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw \DateTime object will be returned.
     *
     * @return mixed Formatted date/time value as string or \DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = NULL)
    {
        if ($format === null) {
            return $this->created;
        } else {
            return $this->created instanceof \DateTime ? $this->created->format($format) : null;
        }
    }

    /**
     * Get the [facebook_id] column value.
     *
     * @return   int
     */
    public function getFacebookId()
    {

        return $this->facebook_id;
    }

    /**
     * Get the [vip] column value.
     *
     * @return   boolean
     */
    public function getVip()
    {

        return $this->vip;
    }

    /**
     * Get the [fail_access] column value.
     *
     * @return   int
     */
    public function getFailAccess()
    {

        return $this->fail_access;
    }

    /**
     * Get the [first_access] column value.
     *
     * @return   boolean
     */
    public function getFirstAccess()
    {

        return $this->first_access;
    }

    /**
     * Get the [optionally formatted] temporal [last_access] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw \DateTime object will be returned.
     *
     * @return mixed Formatted date/time value as string or \DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastAccess($format = NULL)
    {
        if ($format === null) {
            return $this->last_access;
        } else {
            return $this->last_access instanceof \DateTime ? $this->last_access->format($format) : null;
        }
    }

    /**
     * Get the [age] column value.
     *
     * @return   int
     */
    public function getAge()
    {

        return $this->age;
    }

    /**
     * Get the [height] column value.
     *
     * @return   double
     */
    public function getHeight()
    {

        return $this->height;
    }

    /**
     * Get the [weight] column value.
     *
     * @return   double
     */
    public function getWeight()
    {

        return $this->weight;
    }

    /**
     * Get the [sport] column value.
     *
     * @return   double
     */
    public function getSport()
    {

        return $this->sport;
    }

    /**
     * Get the [gender] column value.
     *
     * @return   string
     */
    public function getGender()
    {

        return $this->gender;
    }

    /**
     * Get the [bmr] column value.
     *
     * @return   double
     */
    public function getBmr()
    {

        return $this->bmr;
    }

    /**
     * Get the [program] column value.
     *
     * @return   int
     */
    public function getProgram()
    {

        return $this->program;
    }

    /**
     * Set the value of [id] column.
     *
     * @param      int $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = UserTableMap::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param      string $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = UserTableMap::NAME;
        }


        return $this;
    } // setName()

    /**
     * Set the value of [login] column.
     *
     * @param      string $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[] = UserTableMap::LOGIN;
        }


        return $this;
    } // setLogin()

    /**
     * Set the value of [email] column.
     *
     * @param      string $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = UserTableMap::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [password] column.
     *
     * @param      string $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = UserTableMap::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param      mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return   \User The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, '\DateTime');
        if ($this->created !== null || $dt !== null) {
            if ($dt !== $this->created) {
                $this->created = $dt;
                $this->modifiedColumns[] = UserTableMap::CREATED;
            }
        } // if either are not null


        return $this;
    } // setCreated()

    /**
     * Set the value of [facebook_id] column.
     *
     * @param      int $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setFacebookId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->facebook_id !== $v) {
            $this->facebook_id = $v;
            $this->modifiedColumns[] = UserTableMap::FACEBOOK_ID;
        }


        return $this;
    } // setFacebookId()

    /**
     * Sets the value of the [vip] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param      boolean|integer|string $v The new value
     * @return   \User The current object (for fluent API support)
     */
    public function setVip($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->vip !== $v) {
            $this->vip = $v;
            $this->modifiedColumns[] = UserTableMap::VIP;
        }


        return $this;
    } // setVip()

    /**
     * Set the value of [fail_access] column.
     *
     * @param      int $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setFailAccess($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fail_access !== $v) {
            $this->fail_access = $v;
            $this->modifiedColumns[] = UserTableMap::FAIL_ACCESS;
        }


        return $this;
    } // setFailAccess()

    /**
     * Sets the value of the [first_access] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param      boolean|integer|string $v The new value
     * @return   \User The current object (for fluent API support)
     */
    public function setFirstAccess($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->first_access !== $v) {
            $this->first_access = $v;
            $this->modifiedColumns[] = UserTableMap::FIRST_ACCESS;
        }


        return $this;
    } // setFirstAccess()

    /**
     * Sets the value of [last_access] column to a normalized version of the date/time value specified.
     *
     * @param      mixed $v string, integer (timestamp), or \DateTime value.
     *               Empty strings are treated as NULL.
     * @return   \User The current object (for fluent API support)
     */
    public function setLastAccess($v)
    {
        $dt = PropelDateTime::newInstance($v, null, '\DateTime');
        if ($this->last_access !== null || $dt !== null) {
            if ($dt !== $this->last_access) {
                $this->last_access = $dt;
                $this->modifiedColumns[] = UserTableMap::LAST_ACCESS;
            }
        } // if either are not null


        return $this;
    } // setLastAccess()

    /**
     * Set the value of [age] column.
     *
     * @param      int $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setAge($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->age !== $v) {
            $this->age = $v;
            $this->modifiedColumns[] = UserTableMap::AGE;
        }


        return $this;
    } // setAge()

    /**
     * Set the value of [height] column.
     *
     * @param      double $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setHeight($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->height !== $v) {
            $this->height = $v;
            $this->modifiedColumns[] = UserTableMap::HEIGHT;
        }


        return $this;
    } // setHeight()

    /**
     * Set the value of [weight] column.
     *
     * @param      double $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setWeight($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->weight !== $v) {
            $this->weight = $v;
            $this->modifiedColumns[] = UserTableMap::WEIGHT;
        }


        return $this;
    } // setWeight()

    /**
     * Set the value of [sport] column.
     *
     * @param      double $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setSport($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->sport !== $v) {
            $this->sport = $v;
            $this->modifiedColumns[] = UserTableMap::SPORT;
        }


        return $this;
    } // setSport()

    /**
     * Set the value of [gender] column.
     *
     * @param      string $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[] = UserTableMap::GENDER;
        }


        return $this;
    } // setGender()

    /**
     * Set the value of [bmr] column.
     *
     * @param      double $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setBmr($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->bmr !== $v) {
            $this->bmr = $v;
            $this->modifiedColumns[] = UserTableMap::BMR;
        }


        return $this;
    } // setBmr()

    /**
     * Set the value of [program] column.
     *
     * @param      int $v new value
     * @return   \User The current object (for fluent API support)
     */
    public function setProgram($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->program !== $v) {
            $this->program = $v;
            $this->modifiedColumns[] = UserTableMap::PROGRAM;
        }


        return $this;
    } // setProgram()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->vip !== false) {
                return false;
            }

            if ($this->fail_access !== 0) {
                return false;
            }

            if ($this->first_access !== true) {
                return false;
            }

            if ($this->gender !== 'M') {
                return false;
            }

            if ($this->program !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {


            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserTableMap::translateFieldName('Login', TableMap::TYPE_PHPNAME, $indexType)];
            $this->login = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserTableMap::translateFieldName('Created', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created = (null !== $col) ? PropelDateTime::newInstance($col, null, '\DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserTableMap::translateFieldName('FacebookId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->facebook_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserTableMap::translateFieldName('Vip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vip = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserTableMap::translateFieldName('FailAccess', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fail_access = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserTableMap::translateFieldName('FirstAccess', TableMap::TYPE_PHPNAME, $indexType)];
            $this->first_access = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UserTableMap::translateFieldName('LastAccess', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->last_access = (null !== $col) ? PropelDateTime::newInstance($col, null, '\DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UserTableMap::translateFieldName('Age', TableMap::TYPE_PHPNAME, $indexType)];
            $this->age = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UserTableMap::translateFieldName('Height', TableMap::TYPE_PHPNAME, $indexType)];
            $this->height = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : UserTableMap::translateFieldName('Weight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weight = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : UserTableMap::translateFieldName('Sport', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sport = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : UserTableMap::translateFieldName('Gender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gender = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : UserTableMap::translateFieldName('Bmr', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bmr = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : UserTableMap::translateFieldName('Program', TableMap::TYPE_PHPNAME, $indexType)];
            $this->program = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = UserTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating \User object", 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collPlates = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see User::setDeleted()
     * @see User::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ChildUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->platesScheduledForDeletion !== null) {
                if (!$this->platesScheduledForDeletion->isEmpty()) {
                    foreach ($this->platesScheduledForDeletion as $plate) {
                        // need to save related object because we set the relation to null
                        $plate->save($con);
                    }
                    $this->platesScheduledForDeletion = null;
                }
            }

                if ($this->collPlates !== null) {
            foreach ($this->collPlates as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = UserTableMap::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserTableMap::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserTableMap::ID)) {
            $modifiedColumns[':p' . $index++]  = 'ID';
        }
        if ($this->isColumnModified(UserTableMap::NAME)) {
            $modifiedColumns[':p' . $index++]  = 'NAME';
        }
        if ($this->isColumnModified(UserTableMap::LOGIN)) {
            $modifiedColumns[':p' . $index++]  = 'LOGIN';
        }
        if ($this->isColumnModified(UserTableMap::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'EMAIL';
        }
        if ($this->isColumnModified(UserTableMap::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'PASSWORD';
        }
        if ($this->isColumnModified(UserTableMap::CREATED)) {
            $modifiedColumns[':p' . $index++]  = 'CREATED';
        }
        if ($this->isColumnModified(UserTableMap::FACEBOOK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'FACEBOOK_ID';
        }
        if ($this->isColumnModified(UserTableMap::VIP)) {
            $modifiedColumns[':p' . $index++]  = 'VIP';
        }
        if ($this->isColumnModified(UserTableMap::FAIL_ACCESS)) {
            $modifiedColumns[':p' . $index++]  = 'FAIL_ACCESS';
        }
        if ($this->isColumnModified(UserTableMap::FIRST_ACCESS)) {
            $modifiedColumns[':p' . $index++]  = 'FIRST_ACCESS';
        }
        if ($this->isColumnModified(UserTableMap::LAST_ACCESS)) {
            $modifiedColumns[':p' . $index++]  = 'LAST_ACCESS';
        }
        if ($this->isColumnModified(UserTableMap::AGE)) {
            $modifiedColumns[':p' . $index++]  = 'AGE';
        }
        if ($this->isColumnModified(UserTableMap::HEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'HEIGHT';
        }
        if ($this->isColumnModified(UserTableMap::WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'WEIGHT';
        }
        if ($this->isColumnModified(UserTableMap::SPORT)) {
            $modifiedColumns[':p' . $index++]  = 'SPORT';
        }
        if ($this->isColumnModified(UserTableMap::GENDER)) {
            $modifiedColumns[':p' . $index++]  = 'GENDER';
        }
        if ($this->isColumnModified(UserTableMap::BMR)) {
            $modifiedColumns[':p' . $index++]  = 'BMR';
        }
        if ($this->isColumnModified(UserTableMap::PROGRAM)) {
            $modifiedColumns[':p' . $index++]  = 'PROGRAM';
        }

        $sql = sprintf(
            'INSERT INTO user (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'ID':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'NAME':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'LOGIN':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case 'EMAIL':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'PASSWORD':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'CREATED':
                        $stmt->bindValue($identifier, $this->created ? $this->created->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'FACEBOOK_ID':
                        $stmt->bindValue($identifier, $this->facebook_id, PDO::PARAM_INT);
                        break;
                    case 'VIP':
                        $stmt->bindValue($identifier, (int) $this->vip, PDO::PARAM_INT);
                        break;
                    case 'FAIL_ACCESS':
                        $stmt->bindValue($identifier, $this->fail_access, PDO::PARAM_INT);
                        break;
                    case 'FIRST_ACCESS':
                        $stmt->bindValue($identifier, (int) $this->first_access, PDO::PARAM_INT);
                        break;
                    case 'LAST_ACCESS':
                        $stmt->bindValue($identifier, $this->last_access ? $this->last_access->format("Y-m-d H:i:s") : null, PDO::PARAM_STR);
                        break;
                    case 'AGE':
                        $stmt->bindValue($identifier, $this->age, PDO::PARAM_INT);
                        break;
                    case 'HEIGHT':
                        $stmt->bindValue($identifier, $this->height, PDO::PARAM_STR);
                        break;
                    case 'WEIGHT':
                        $stmt->bindValue($identifier, $this->weight, PDO::PARAM_STR);
                        break;
                    case 'SPORT':
                        $stmt->bindValue($identifier, $this->sport, PDO::PARAM_STR);
                        break;
                    case 'GENDER':
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case 'BMR':
                        $stmt->bindValue($identifier, $this->bmr, PDO::PARAM_STR);
                        break;
                    case 'PROGRAM':
                        $stmt->bindValue($identifier, $this->program, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getLogin();
                break;
            case 3:
                return $this->getEmail();
                break;
            case 4:
                return $this->getPassword();
                break;
            case 5:
                return $this->getCreated();
                break;
            case 6:
                return $this->getFacebookId();
                break;
            case 7:
                return $this->getVip();
                break;
            case 8:
                return $this->getFailAccess();
                break;
            case 9:
                return $this->getFirstAccess();
                break;
            case 10:
                return $this->getLastAccess();
                break;
            case 11:
                return $this->getAge();
                break;
            case 12:
                return $this->getHeight();
                break;
            case 13:
                return $this->getWeight();
                break;
            case 14:
                return $this->getSport();
                break;
            case 15:
                return $this->getGender();
                break;
            case 16:
                return $this->getBmr();
                break;
            case 17:
                return $this->getProgram();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['User'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['User'][$this->getPrimaryKey()] = true;
        $keys = UserTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getLogin(),
            $keys[3] => $this->getEmail(),
            $keys[4] => $this->getPassword(),
            $keys[5] => $this->getCreated(),
            $keys[6] => $this->getFacebookId(),
            $keys[7] => $this->getVip(),
            $keys[8] => $this->getFailAccess(),
            $keys[9] => $this->getFirstAccess(),
            $keys[10] => $this->getLastAccess(),
            $keys[11] => $this->getAge(),
            $keys[12] => $this->getHeight(),
            $keys[13] => $this->getWeight(),
            $keys[14] => $this->getSport(),
            $keys[15] => $this->getGender(),
            $keys[16] => $this->getBmr(),
            $keys[17] => $this->getProgram(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collPlates) {
                $result['Plates'] = $this->collPlates->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param      string $name
     * @param      mixed  $value field value
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return void
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @param      mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setLogin($value);
                break;
            case 3:
                $this->setEmail($value);
                break;
            case 4:
                $this->setPassword($value);
                break;
            case 5:
                $this->setCreated($value);
                break;
            case 6:
                $this->setFacebookId($value);
                break;
            case 7:
                $this->setVip($value);
                break;
            case 8:
                $this->setFailAccess($value);
                break;
            case 9:
                $this->setFirstAccess($value);
                break;
            case 10:
                $this->setLastAccess($value);
                break;
            case 11:
                $this->setAge($value);
                break;
            case 12:
                $this->setHeight($value);
                break;
            case 13:
                $this->setWeight($value);
                break;
            case 14:
                $this->setSport($value);
                break;
            case 15:
                $this->setGender($value);
                break;
            case 16:
                $this->setBmr($value);
                break;
            case 17:
                $this->setProgram($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setLogin($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEmail($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPassword($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCreated($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setFacebookId($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setVip($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFailAccess($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setFirstAccess($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLastAccess($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAge($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setHeight($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setWeight($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setSport($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setGender($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setBmr($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setProgram($arr[$keys[17]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserTableMap::ID)) $criteria->add(UserTableMap::ID, $this->id);
        if ($this->isColumnModified(UserTableMap::NAME)) $criteria->add(UserTableMap::NAME, $this->name);
        if ($this->isColumnModified(UserTableMap::LOGIN)) $criteria->add(UserTableMap::LOGIN, $this->login);
        if ($this->isColumnModified(UserTableMap::EMAIL)) $criteria->add(UserTableMap::EMAIL, $this->email);
        if ($this->isColumnModified(UserTableMap::PASSWORD)) $criteria->add(UserTableMap::PASSWORD, $this->password);
        if ($this->isColumnModified(UserTableMap::CREATED)) $criteria->add(UserTableMap::CREATED, $this->created);
        if ($this->isColumnModified(UserTableMap::FACEBOOK_ID)) $criteria->add(UserTableMap::FACEBOOK_ID, $this->facebook_id);
        if ($this->isColumnModified(UserTableMap::VIP)) $criteria->add(UserTableMap::VIP, $this->vip);
        if ($this->isColumnModified(UserTableMap::FAIL_ACCESS)) $criteria->add(UserTableMap::FAIL_ACCESS, $this->fail_access);
        if ($this->isColumnModified(UserTableMap::FIRST_ACCESS)) $criteria->add(UserTableMap::FIRST_ACCESS, $this->first_access);
        if ($this->isColumnModified(UserTableMap::LAST_ACCESS)) $criteria->add(UserTableMap::LAST_ACCESS, $this->last_access);
        if ($this->isColumnModified(UserTableMap::AGE)) $criteria->add(UserTableMap::AGE, $this->age);
        if ($this->isColumnModified(UserTableMap::HEIGHT)) $criteria->add(UserTableMap::HEIGHT, $this->height);
        if ($this->isColumnModified(UserTableMap::WEIGHT)) $criteria->add(UserTableMap::WEIGHT, $this->weight);
        if ($this->isColumnModified(UserTableMap::SPORT)) $criteria->add(UserTableMap::SPORT, $this->sport);
        if ($this->isColumnModified(UserTableMap::GENDER)) $criteria->add(UserTableMap::GENDER, $this->gender);
        if ($this->isColumnModified(UserTableMap::BMR)) $criteria->add(UserTableMap::BMR, $this->bmr);
        if ($this->isColumnModified(UserTableMap::PROGRAM)) $criteria->add(UserTableMap::PROGRAM, $this->program);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(UserTableMap::DATABASE_NAME);
        $criteria->add(UserTableMap::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return   int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \User (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setFacebookId($this->getFacebookId());
        $copyObj->setVip($this->getVip());
        $copyObj->setFailAccess($this->getFailAccess());
        $copyObj->setFirstAccess($this->getFirstAccess());
        $copyObj->setLastAccess($this->getLastAccess());
        $copyObj->setAge($this->getAge());
        $copyObj->setHeight($this->getHeight());
        $copyObj->setWeight($this->getWeight());
        $copyObj->setSport($this->getSport());
        $copyObj->setGender($this->getGender());
        $copyObj->setBmr($this->getBmr());
        $copyObj->setProgram($this->getProgram());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getPlates() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPlate($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return                 \User Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Plate' == $relationName) {
            return $this->initPlates();
        }
    }

    /**
     * Clears out the collPlates collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPlates()
     */
    public function clearPlates()
    {
        $this->collPlates = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPlates collection loaded partially.
     */
    public function resetPartialPlates($v = true)
    {
        $this->collPlatesPartial = $v;
    }

    /**
     * Initializes the collPlates collection.
     *
     * By default this just sets the collPlates collection to an empty array (like clearcollPlates());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPlates($overrideExisting = true)
    {
        if (null !== $this->collPlates && !$overrideExisting) {
            return;
        }
        $this->collPlates = new ObjectCollection();
        $this->collPlates->setModel('\Plate');
    }

    /**
     * Gets an array of ChildPlate objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return Collection|ChildPlate[] List of ChildPlate objects
     * @throws PropelException
     */
    public function getPlates($criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPlatesPartial && !$this->isNew();
        if (null === $this->collPlates || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collPlates) {
                // return empty collection
                $this->initPlates();
            } else {
                $collPlates = ChildPlateQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPlatesPartial && count($collPlates)) {
                        $this->initPlates(false);

                        foreach ($collPlates as $obj) {
                            if (false == $this->collPlates->contains($obj)) {
                                $this->collPlates->append($obj);
                            }
                        }

                        $this->collPlatesPartial = true;
                    }

                    $collPlates->getInternalIterator()->rewind();

                    return $collPlates;
                }

                if ($partial && $this->collPlates) {
                    foreach ($this->collPlates as $obj) {
                        if ($obj->isNew()) {
                            $collPlates[] = $obj;
                        }
                    }
                }

                $this->collPlates = $collPlates;
                $this->collPlatesPartial = false;
            }
        }

        return $this->collPlates;
    }

    /**
     * Sets a collection of Plate objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $plates A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return   ChildUser The current object (for fluent API support)
     */
    public function setPlates(Collection $plates, ConnectionInterface $con = null)
    {
        $platesToDelete = $this->getPlates(new Criteria(), $con)->diff($plates);


        $this->platesScheduledForDeletion = $platesToDelete;

        foreach ($platesToDelete as $plateRemoved) {
            $plateRemoved->setUser(null);
        }

        $this->collPlates = null;
        foreach ($plates as $plate) {
            $this->addPlate($plate);
        }

        $this->collPlates = $plates;
        $this->collPlatesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Plate objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Plate objects.
     * @throws PropelException
     */
    public function countPlates(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPlatesPartial && !$this->isNew();
        if (null === $this->collPlates || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPlates) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPlates());
            }

            $query = ChildPlateQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collPlates);
    }

    /**
     * Method called to associate a ChildPlate object to this object
     * through the ChildPlate foreign key attribute.
     *
     * @param    ChildPlate $l ChildPlate
     * @return   \User The current object (for fluent API support)
     */
    public function addPlate(ChildPlate $l)
    {
        if ($this->collPlates === null) {
            $this->initPlates();
            $this->collPlatesPartial = true;
        }

        if (!in_array($l, $this->collPlates->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddPlate($l);
        }

        return $this;
    }

    /**
     * @param Plate $plate The plate object to add.
     */
    protected function doAddPlate($plate)
    {
        $this->collPlates[]= $plate;
        $plate->setUser($this);
    }

    /**
     * @param  Plate $plate The plate object to remove.
     * @return ChildUser The current object (for fluent API support)
     */
    public function removePlate($plate)
    {
        if ($this->getPlates()->contains($plate)) {
            $this->collPlates->remove($this->collPlates->search($plate));
            if (null === $this->platesScheduledForDeletion) {
                $this->platesScheduledForDeletion = clone $this->collPlates;
                $this->platesScheduledForDeletion->clear();
            }
            $this->platesScheduledForDeletion[]= $plate;
            $plate->setUser(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->login = null;
        $this->email = null;
        $this->password = null;
        $this->created = null;
        $this->facebook_id = null;
        $this->vip = null;
        $this->fail_access = null;
        $this->first_access = null;
        $this->last_access = null;
        $this->age = null;
        $this->height = null;
        $this->weight = null;
        $this->sport = null;
        $this->gender = null;
        $this->bmr = null;
        $this->program = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collPlates) {
                foreach ($this->collPlates as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        if ($this->collPlates instanceof Collection) {
            $this->collPlates->clearIterator();
        }
        $this->collPlates = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {

    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {

    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
