<?php

use Base\FoodQuery as BaseFoodQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'food' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FoodQuery extends BaseFoodQuery
{
    //FIXME: tato metoda mi nefunguje a ani to nepouzivam
    public function toJSONArray() {
        $foods = $this->find();
        $foodArray = array();
        foreach($foods as $food){
            array_push($foodArray, $food->toArray());
        }
        return json_encode($foodArray);
    }

    /**
     * Mapovanie hodnot propel na moju strukturu JSON
     * */
    public function toMyArray($foods){
        $data = array();

        if (count($foods)>=1)
            foreach ($foods as $food)
                $data[] = $this->mapping($food);
        else
            $data[] = $this->mapping($foods);

        return $data;
    }

    private function mapping($food){
        if ($food instanceof Food){
            $data = array('id'=>$food->getId(),
                'name'=>$food->getName(),
                'amount'=>$food->getAmount(),
                'energy_kj'=>$food->getEnergyKj(),
                'energy_kcal'=>$food->getEnergyKcal(),
                'protein'=>$food->getProtein(),
                'fat'=>$food->getFat(),
                'sugar'=>$food->getSugar(),
                'category'=>$food->getCategory(),
                'tags'=>$food->getTags(),
                'fluid'=>$food->getFluid());
            return $data;
        } else
            return null;
    }

} // FoodQuery
