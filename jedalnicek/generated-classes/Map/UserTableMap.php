<?php

namespace Map;

use \User;
use \UserQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;
    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.UserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'jedalnicek';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\User';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'User';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the ID field
     */
    const ID = 'user.ID';

    /**
     * the column name for the NAME field
     */
    const NAME = 'user.NAME';

    /**
     * the column name for the LOGIN field
     */
    const LOGIN = 'user.LOGIN';

    /**
     * the column name for the EMAIL field
     */
    const EMAIL = 'user.EMAIL';

    /**
     * the column name for the PASSWORD field
     */
    const PASSWORD = 'user.PASSWORD';

    /**
     * the column name for the CREATED field
     */
    const CREATED = 'user.CREATED';

    /**
     * the column name for the FACEBOOK_ID field
     */
    const FACEBOOK_ID = 'user.FACEBOOK_ID';

    /**
     * the column name for the VIP field
     */
    const VIP = 'user.VIP';

    /**
     * the column name for the FAIL_ACCESS field
     */
    const FAIL_ACCESS = 'user.FAIL_ACCESS';

    /**
     * the column name for the FIRST_ACCESS field
     */
    const FIRST_ACCESS = 'user.FIRST_ACCESS';

    /**
     * the column name for the LAST_ACCESS field
     */
    const LAST_ACCESS = 'user.LAST_ACCESS';

    /**
     * the column name for the AGE field
     */
    const AGE = 'user.AGE';

    /**
     * the column name for the HEIGHT field
     */
    const HEIGHT = 'user.HEIGHT';

    /**
     * the column name for the WEIGHT field
     */
    const WEIGHT = 'user.WEIGHT';

    /**
     * the column name for the SPORT field
     */
    const SPORT = 'user.SPORT';

    /**
     * the column name for the GENDER field
     */
    const GENDER = 'user.GENDER';

    /**
     * the column name for the BMR field
     */
    const BMR = 'user.BMR';

    /**
     * the column name for the PROGRAM field
     */
    const PROGRAM = 'user.PROGRAM';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Login', 'Email', 'Password', 'Created', 'FacebookId', 'Vip', 'FailAccess', 'FirstAccess', 'LastAccess', 'Age', 'Height', 'Weight', 'Sport', 'Gender', 'Bmr', 'Program', ),
        self::TYPE_STUDLYPHPNAME => array('id', 'name', 'login', 'email', 'password', 'created', 'facebookId', 'vip', 'failAccess', 'firstAccess', 'lastAccess', 'age', 'height', 'weight', 'sport', 'gender', 'bmr', 'program', ),
        self::TYPE_COLNAME       => array(UserTableMap::ID, UserTableMap::NAME, UserTableMap::LOGIN, UserTableMap::EMAIL, UserTableMap::PASSWORD, UserTableMap::CREATED, UserTableMap::FACEBOOK_ID, UserTableMap::VIP, UserTableMap::FAIL_ACCESS, UserTableMap::FIRST_ACCESS, UserTableMap::LAST_ACCESS, UserTableMap::AGE, UserTableMap::HEIGHT, UserTableMap::WEIGHT, UserTableMap::SPORT, UserTableMap::GENDER, UserTableMap::BMR, UserTableMap::PROGRAM, ),
        self::TYPE_RAW_COLNAME   => array('ID', 'NAME', 'LOGIN', 'EMAIL', 'PASSWORD', 'CREATED', 'FACEBOOK_ID', 'VIP', 'FAIL_ACCESS', 'FIRST_ACCESS', 'LAST_ACCESS', 'AGE', 'HEIGHT', 'WEIGHT', 'SPORT', 'GENDER', 'BMR', 'PROGRAM', ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'login', 'email', 'password', 'created', 'facebook_id', 'vip', 'fail_access', 'first_access', 'last_access', 'age', 'height', 'weight', 'sport', 'gender', 'bmr', 'program', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Login' => 2, 'Email' => 3, 'Password' => 4, 'Created' => 5, 'FacebookId' => 6, 'Vip' => 7, 'FailAccess' => 8, 'FirstAccess' => 9, 'LastAccess' => 10, 'Age' => 11, 'Height' => 12, 'Weight' => 13, 'Sport' => 14, 'Gender' => 15, 'Bmr' => 16, 'Program' => 17, ),
        self::TYPE_STUDLYPHPNAME => array('id' => 0, 'name' => 1, 'login' => 2, 'email' => 3, 'password' => 4, 'created' => 5, 'facebookId' => 6, 'vip' => 7, 'failAccess' => 8, 'firstAccess' => 9, 'lastAccess' => 10, 'age' => 11, 'height' => 12, 'weight' => 13, 'sport' => 14, 'gender' => 15, 'bmr' => 16, 'program' => 17, ),
        self::TYPE_COLNAME       => array(UserTableMap::ID => 0, UserTableMap::NAME => 1, UserTableMap::LOGIN => 2, UserTableMap::EMAIL => 3, UserTableMap::PASSWORD => 4, UserTableMap::CREATED => 5, UserTableMap::FACEBOOK_ID => 6, UserTableMap::VIP => 7, UserTableMap::FAIL_ACCESS => 8, UserTableMap::FIRST_ACCESS => 9, UserTableMap::LAST_ACCESS => 10, UserTableMap::AGE => 11, UserTableMap::HEIGHT => 12, UserTableMap::WEIGHT => 13, UserTableMap::SPORT => 14, UserTableMap::GENDER => 15, UserTableMap::BMR => 16, UserTableMap::PROGRAM => 17, ),
        self::TYPE_RAW_COLNAME   => array('ID' => 0, 'NAME' => 1, 'LOGIN' => 2, 'EMAIL' => 3, 'PASSWORD' => 4, 'CREATED' => 5, 'FACEBOOK_ID' => 6, 'VIP' => 7, 'FAIL_ACCESS' => 8, 'FIRST_ACCESS' => 9, 'LAST_ACCESS' => 10, 'AGE' => 11, 'HEIGHT' => 12, 'WEIGHT' => 13, 'SPORT' => 14, 'GENDER' => 15, 'BMR' => 16, 'PROGRAM' => 17, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'login' => 2, 'email' => 3, 'password' => 4, 'created' => 5, 'facebook_id' => 6, 'vip' => 7, 'fail_access' => 8, 'first_access' => 9, 'last_access' => 10, 'age' => 11, 'height' => 12, 'weight' => 13, 'sport' => 14, 'gender' => 15, 'bmr' => 16, 'program' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user');
        $this->setPhpName('User');
        $this->setClassName('\\User');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('NAME', 'Name', 'VARCHAR', true, 64, null);
        $this->addColumn('LOGIN', 'Login', 'VARCHAR', true, 64, null);
        $this->addColumn('EMAIL', 'Email', 'VARCHAR', true, 64, null);
        $this->addColumn('PASSWORD', 'Password', 'VARCHAR', true, 32, null);
        $this->addColumn('CREATED', 'Created', 'TIMESTAMP', true, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('FACEBOOK_ID', 'FacebookId', 'INTEGER', false, null, null);
        $this->addColumn('VIP', 'Vip', 'BOOLEAN', false, 1, false);
        $this->addColumn('FAIL_ACCESS', 'FailAccess', 'INTEGER', false, null, 0);
        $this->addColumn('FIRST_ACCESS', 'FirstAccess', 'BOOLEAN', false, 1, true);
        $this->addColumn('LAST_ACCESS', 'LastAccess', 'TIMESTAMP', false, null, null);
        $this->addColumn('AGE', 'Age', 'INTEGER', false, null, null);
        $this->addColumn('HEIGHT', 'Height', 'FLOAT', false, 4,1, null);
        $this->addColumn('WEIGHT', 'Weight', 'FLOAT', false, 4,1, null);
        $this->addColumn('SPORT', 'Sport', 'FLOAT', false, 4,1, null);
        $this->addColumn('GENDER', 'Gender', 'VARCHAR', false, 1, 'M');
        $this->addColumn('BMR', 'Bmr', 'FLOAT', false, 4,1, null);
        $this->addColumn('PROGRAM', 'Program', 'INTEGER', false, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Plate', '\\Plate', RelationMap::ONE_TO_MANY, array('id' => 'user_id', ), null, null, 'Plates');
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {

            return (int) $row[
                            $indexType == TableMap::TYPE_NUM
                            ? 0 + $offset
                            : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
                        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserTableMap::CLASS_DEFAULT : UserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_STUDLYPHPNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     * @return array (User object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserTableMap::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserTableMap::ID);
            $criteria->addSelectColumn(UserTableMap::NAME);
            $criteria->addSelectColumn(UserTableMap::LOGIN);
            $criteria->addSelectColumn(UserTableMap::EMAIL);
            $criteria->addSelectColumn(UserTableMap::PASSWORD);
            $criteria->addSelectColumn(UserTableMap::CREATED);
            $criteria->addSelectColumn(UserTableMap::FACEBOOK_ID);
            $criteria->addSelectColumn(UserTableMap::VIP);
            $criteria->addSelectColumn(UserTableMap::FAIL_ACCESS);
            $criteria->addSelectColumn(UserTableMap::FIRST_ACCESS);
            $criteria->addSelectColumn(UserTableMap::LAST_ACCESS);
            $criteria->addSelectColumn(UserTableMap::AGE);
            $criteria->addSelectColumn(UserTableMap::HEIGHT);
            $criteria->addSelectColumn(UserTableMap::WEIGHT);
            $criteria->addSelectColumn(UserTableMap::SPORT);
            $criteria->addSelectColumn(UserTableMap::GENDER);
            $criteria->addSelectColumn(UserTableMap::BMR);
            $criteria->addSelectColumn(UserTableMap::PROGRAM);
        } else {
            $criteria->addSelectColumn($alias . '.ID');
            $criteria->addSelectColumn($alias . '.NAME');
            $criteria->addSelectColumn($alias . '.LOGIN');
            $criteria->addSelectColumn($alias . '.EMAIL');
            $criteria->addSelectColumn($alias . '.PASSWORD');
            $criteria->addSelectColumn($alias . '.CREATED');
            $criteria->addSelectColumn($alias . '.FACEBOOK_ID');
            $criteria->addSelectColumn($alias . '.VIP');
            $criteria->addSelectColumn($alias . '.FAIL_ACCESS');
            $criteria->addSelectColumn($alias . '.FIRST_ACCESS');
            $criteria->addSelectColumn($alias . '.LAST_ACCESS');
            $criteria->addSelectColumn($alias . '.AGE');
            $criteria->addSelectColumn($alias . '.HEIGHT');
            $criteria->addSelectColumn($alias . '.WEIGHT');
            $criteria->addSelectColumn($alias . '.SPORT');
            $criteria->addSelectColumn($alias . '.GENDER');
            $criteria->addSelectColumn($alias . '.BMR');
            $criteria->addSelectColumn($alias . '.PROGRAM');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserTableMap::DATABASE_NAME)->getTable(UserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserTableMap::DATABASE_NAME);
      if (!$dbMap->hasTable(UserTableMap::TABLE_NAME)) {
        $dbMap->addTableObject(new UserTableMap());
      }
    }

    /**
     * Performs a DELETE on the database, given a User or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or User object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \User) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserTableMap::DATABASE_NAME);
            $criteria->add(UserTableMap::ID, (array) $values, Criteria::IN);
        }

        $query = UserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) { UserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) { UserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a User or Criteria object.
     *
     * @param mixed               $criteria Criteria or User object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from User object
        }

        if ($criteria->containsKey(UserTableMap::ID) && $criteria->keyContainsValue(UserTableMap::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserTableMap::ID.')');
        }


        // Set the correct dbName
        $query = UserQuery::create()->mergeWith($criteria);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = $query->doInsert($con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

} // UserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserTableMap::buildTableMap();
