<?php

use Base\PlateQuery as BasePlateQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'plate' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PlateQuery extends BasePlateQuery
{
    /**
     * Mapovanie hodnot propel na moju strukturu JSON
     * */
    public function toMyArray($plates){
        $data = array();

        if (count($plates)>=1)
            foreach ($plates as $plate)
                $data[] = $this->mapping($plate);
        else
            $data[] = $this->mapping($plates);

        return $data;
    }

    public function getDistinctPlateByName($name = '%'){
        $plates = self::create()
            ->setDistinct('name')
            ->filterByName($name)
            ->filterByName( '', Propel\Runtime\ActiveQuery\Criteria::NOT_EQUAL)
            ->find();

        $data =array();
        foreach ($plates as $plate){
            $data[$plate->getName()] = $plate;
        }
        $plates = array();
        foreach ($data as $d){
            $plates[] = $d;
        }

        return $this->toMyArray($plates);
    }

    private function mapping($plate){
        if ($plate instanceof Plate){

            $data = array();
            $data['id'] =  ($plate->getId()!=null) ? $plate->getId():null;
            $data['name'] = ($plate->getName()!=null) ?  $plate->getName():null;
            $data['date'] = ($plate->getDate()!=null) ?  $plate->getDate():null;
            $data['dish'] = ($plate->getDish()!=null) ?  $plate->getDish():null;
            $data['user_id'] = ($plate->getUserId()!=null) ? $plate->getUserId():null;

           /* $data = array($plate->getId()?'id'=>$plate->getId():,
                'name'=>$plate->getName(),
                'date'=>$plate->getDate(),
                'dish'=>$plate->getDish(),
                'user_id'=>$plate->getUserId());*/
            return $data;
        } else
            return null;
    }
} // PlateQuery
