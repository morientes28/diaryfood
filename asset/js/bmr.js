$(document).ready(function() {

    /* init GUI by parameters ---------------- */
    var user = $.session.get('user');
    $('input[name=user]').val(user);

    $("#bmr").on('click', function(){
        event.preventDefault();
        var form_data = $("#bmr-form").serialize();
        $.ajax({
            type: "POST",
            url: "driver.php",
            dataType: "json",
            data: {service: "bmr-index",  params: {form: form_data}, auth_key: "p7r1AsDkhK6ejBbUbbzb"}
        })
            .done(function( data ) {

                window.location.href ='index.html';

            });

    });

    $("#cancel").on('click', function(){
        event.preventDefault();
        window.location.href ='index.html';
    });
});