
$(document).ready(function() {

    /* login */
    $("#login-form").on('submit', function(){
        event.preventDefault();
        var form_data = $("#login-form").serialize();
        $.ajax({
            type: "POST",
            url: "driver.php",
            dataType: "json",
            data: {service: "login", params: {form: form_data}, auth_key: "p7r1AsDkhK6ejBbUbbzb"}
        })
            .done(function( data ) {

                if (data.result==false){
                    alert('chyba prihlasenia');
                    $.session.clear();
                } else {
                    $.session.set('user', data.result.user);
                    window.location.href ='index.html';
                }
            });

    });

    $("#logout").on('click', function(){
        $.session.clear();
        window.location.href ='login.html';
    });
});
