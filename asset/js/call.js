/**
 * call external function by ajax
 *
 * METHOD:
 * all, by-id, by-name, by-params,
 *
 * RETURN:
 * json {
        "result": [
                    {
                        "id": "3",
                        "name": "brav\u010dov\u00e1 krkovi\u010dka",
                        "amount": "100 g",
                        "energy_kj": "999.9",
                        ...
                    }
                  ]
        }
 *
 * POST:
 * ... driver.php
 *
 * GET:
 * ... driver.php?service=by-id&params%5Bid%5D%5B%5D=3&params%5Bid%5D%5B%5D=4&authkey=p7r1AsDkhK6ejBbUbbzb
 * ... driver.php?service=by-name&name=chlieb&authkey=p7r1AsDkhK6ejBbUbbzb
 *
 */

$(document).ready(function() {

    /* init GUI by parameters ---------------- */
    var day = getParameterByName('d');
    var dish = getParameterByName('p');
    if (day==''){
        var d = new Date();
        day = d.getFullYear()+'-'+eval(d.getMonth()+1)+'-'+d.getDate();
    }
    $.session.set('day', day);
    var strDay = day.split("-");
    $('#date').html(strDay[2]+'.'+strDay[1]+'.'+strDay[0]);

    if (dish==''){
        var dish = $.session.get('dish');
        if (dish==null){
            $.session.set('dish', 'breakfast');
        }
    } else {
        $.session.set('dish', dish);
        location.search = location.search.replace(/&p=[^&;]*/,'');
    }

    var user = $.session.get('user');

    if (user==null){
        window.location.href ='login.html';
    }

    // access to admin managment
    if (user==1){
        $('#managment-food-templates-div').css('display','visible');
    } else {
        $('#managment-food-templates-div').css('display','none');
    }

    $.ajax({
        type: "POST",
        url: "driver.php",
        dataType: "json",
        data: {service: "init", params: {user : user, date: day, dish: dish}, auth_key: "p7r1AsDkhK6ejBbUbbzb"}
    })
        .done(function( data ) {

            $('#jar table').html('');
            $('#jar table').append('<tr><th>Nazov jedla</th><th>Kalorie [Kj]</th><th>Kalorie [kcal]</th><th>Bielkoviny[g]</th><th>Tuk [g]</th><th>Cukor [g]</th><th>Mnozstvo [g]</th></tr>');

            $.each(data.result,function(index,value){
                if (data.result[index]!=undefined)
                    $('#jar table').append('<tr>'
                        +'<td>'+data.result[index].name+'</td>'
                        +'<td>'+data.result[index].energy_kj +'</td>'
                        +'<td>'+data.result[index].energy_kcal +'</td>'
                        +'<td>'+data.result[index].protein +'</td>'
                        +'<td>'+data.result[index].fat+'</td>'
                        +'<td>'+data.result[index].sugar +'</td>'
                        +'<td><input type="text" id="amount_'+data.result[index].id+'" value="'+data.result[index].amount+'"></td>'
                        +'<td><a href="#" ref="' + data.result[index].id + '" onClick="adjustFood(this)"><span class="glyphicon glyphicon-plus"></a></td>'
                        +'<td><a href="#" ref="' + data.result[index].id + '" onClick="deleteFood(this)"><span class="glyphicon glyphicon-remove"></a></td>'+

                        '</tr>');
            });

            refreshPlate();
            sumFood();
            getTemplates();
        });


	/* init select2 food --------------------- */
    //$("#food-choose-list").select2();
    function format2(food) {
        var originalOption = food.element; 
        if ($(originalOption).data('flag')==undefined) return food.text;
        return "<img class='icons' src='asset/images/icons/" + $(originalOption).data('flag') + ".png'/>&nbsp;&nbsp;&nbsp;" + food.text;
    }
    $("#food-choose-list").select2({
        formatResult: format2,
        formatSelection: format2,
        escapeMarkup: function(m) { return m; }
    });
    //$("#food-category").select2();


    function format(food) {
        var originalOption = food.element; 
        if ($(originalOption).data('flag')=="all") return food.text;
        return "<img class='flags' src='asset/images/flags/" + $(originalOption).data('flag') + ".jpg'/>&nbsp;&nbsp;&nbsp;" + food.text;
    }
    $("#food-category").select2({
        formatResult: format,
        formatSelection: format,
        escapeMarkup: function(m) { return m; }
    });


   // $("#part-of-day").select2();
    $("#templates-foods").select2();

	$.ajax({
		    type: "POST",
		    url: "driver.php",
            dataType: "json",
		    data: {service: "all", auth_key: "p7r1AsDkhK6ejBbUbbzb"}
    })
    .done(function( data ) {

            //data = JSON.parse(data);

            $.each(data.result,function(index,value){
			    $('#food-choose-list').append('<option value="' + value.id+ '" data-flag="'+value.tags+'">'+value.name+'</option>');
			});
	});

    $("#food-category").on('change', function(){
        var category_chosen = $( "#food-category option:selected" ).val();
        $.ajax({
            type: "POST",
            url: "driver.php",
            dataType: "json",
            data: {service: "food-category", params: {category: category_chosen}, auth_key: "p7r1AsDkhK6ejBbUbbzb"}
        })
        .done(function( data ) {
           $('#food-choose-list').html('');
            $('#food-choose-list').append('<option value="">... vyber jedlo ...</option>');
            $.each(data.result,function(index,value){
               $('#food-choose-list').append('<option value="' + value.id+ '" data-flag="'+value.tags+'">'+value.name+'</option>');
            });

        });
    });

	$("#food-choose-list").on('change', function(){
		var id = $('#food-choose-list').val();
		var text = $( "#food-choose-list option:selected" ).text();

		$('#plate').append($('<option>', {
		    value: id,
		    text: text
		}));
        var dish = $.session.get('dish');
        addItemToPlate(id, user, day, dish);
	});

    $("input[name='part-of-day']").on('click', function(){
        var id = $(this).val();
        $.session.set('dish', id);
        refreshPlate();
    });

    $("#food-template-save").on('click', function(){
        var dish = $.session.get('dish');
        var name = $("#food-template-name").val();
        $.ajax({
            type: "POST",
            url: "driver.php",
            dataType: "json",
            data: { service: "add-template", params: {date: day, dish: dish, name: name}, auth_key: "p7r1AsDkhK6ejBbUbbzb" }
        })
            .done(function( data ) {
               alert('Uložená šablóna do databázy');
               window.location.reload();
            });
    });

    $("#templates-foods").on('change', function(){

        var dish = $.session.get('dish');
        var text = $( "#templates-foods option:selected" ).text();

        $.ajax({
            type: "POST",
            url: "driver.php",
            dataType: "json",
            data: { service: "set-template", params: {name : text, user : user, date: day, dish: dish}, auth_key: "p7r1AsDkhK6ejBbUbbzb" }
        })
            .done(function( data ) {
                refreshPlate();
            });

    });

    function addItemToPlate(id, user, day, dish){
        $.ajax({
            type: "POST",
            url: "driver.php",
            dataType: "json",
            data: { service: "add-food-to-plate", params: {id : id, user : user, date: day, dish: dish}, auth_key: "p7r1AsDkhK6ejBbUbbzb" }
        })
            .done(function( data ) {
               // data = JSON.parse(data);

                $('#jar table tr:last').after('<tr>'
                    +'<td>'+data.result[0].name+'</td>'
                    +'<td>'+data.result[0].energy_kj +'</td>'
                    +'<td>'+data.result[0].energy_kcal +'</td>'
                    +'<td>'+data.result[0].protein +'</td>'
                    +'<td>'+data.result[0].fat+'</td>'
                    +'<td>'+data.result[0].sugar +'</td>'
                    +'<td><input type="text" id="amount_'+data.result[0].id+'" value="'+data.result[0].amount+'"></td>'
                    +'<td><a href="#" ref="' + data.result[0].id + '" onClick="adjustFood(this)"><span class="glyphicon glyphicon-plus"></span></a></td>'
                    +'<td><a href="#" ref="' + data.result[0].id + '" onClick="deleteFood(this)"><span class="glyphicon glyphicon-remove"></span></a></td>'+

                                 '</tr>');

                refreshPlate();
                sumFood();
            });
    }

});

/* ----------------------------- */
function renderPlateFoodImages(foods){
    if (foods.length>0){
        $('.food-wrapper').html('');
        $.each(foods,function(index,value){
            if (value!=undefined)
                if (value.tags != null)
                    $('.food-wrapper').append('<img src="asset/images/icons/' + value.tags + '.png" class="food-icon">');

        });
        var w = $('.food-icon').width();
        var h = $('.food-icon').height();
        $('.food-icon').width(w * 2);
        $('.food-icon').height(h * 2);

        if (foods.length > 1 && foods.length < 4){
            $('.food-icon').width(w * 1.80);
            $('.food-icon').height(h * 1.80);
        } else if (foods.length >= 4 && foods.length < 8){
            $('.food-icon').width(w * 1.20);
            $('.food-icon').height(h * 1.20);
        } else if (foods.length >= 8 && foods.length < 12){
            $('.food-icon').width(w * 1);
            $('.food-icon').height(h * 1);
        } else if (foods.length >= 12 && foods.length < 16){
            $('.food-icon').width(w * 0.90);
            $('.food-icon').height(h * 0.90);
        } else if (foods.length >= 16){
            $('.food-icon').width(w * 0.7);
            $('.food-icon').height(h * 0.7);
        }
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function deleteFood(obj){
    //var user = getParameterByName('u');
    var user = $.session.get('user');
    var day = $.session.get('day');
    var dish = $.session.get('dish');
    //var day = getParameterByName('d');
    var id = obj.getAttribute('ref');
    $.ajax({
        type: "POST",
        url: "driver.php",
        dataType: "json",
        data: { service: "delete-food", params: {id : [id], user : user, date: day, dish: dish}, auth_key: "p7r1AsDkhK6ejBbUbbzb" }
    })
    .done(function( data ) {
        refreshPlate();
        sumFood();
    });
}

function adjustFood(obj){
    var user = $.session.get('user');
   // var day = getParameterByName('d');
    var day = $.session.get('day');
    var dish = $.session.get('dish');
    var id = obj.getAttribute('ref');
    var amount = $('#amount_'+id).val();

    $.ajax({
        type: "POST",
        url: "driver.php",
        dataType: "json",
        data: { service: "adjust-food", params: {id : [id], user : user, date: day, amount: amount, dish: dish}, auth_key: "p7r1AsDkhK6ejBbUbbzb" }
    })
        .done(function( data ) {
            refreshPlate();
            sumFood();
        });
}

function getTemplates(){
    $.ajax({
        type: "POST",
        url: "driver.php",
        dataType: "json",
        data: {service: "get-templates", params: {name : '%'}, auth_key: "p7r1AsDkhK6ejBbUbbzb"}
    })
        .done(function( data ) {
            $('#templates-foods').html('');
            $.each(data.result,function(index,value){
               $('#templates-foods').append($('<option>', {
                    value: value.id,
                    text: value.name
                }));
            });
        });
}

function refreshPlate(){
    var user = $.session.get('user');
    //var day = getParameterByName('d');
    var day = $.session.get('day');
    var dish = $.session.get('dish');
    $.ajax({
        type: "POST",
        url: "driver.php",
        dataType: "json",
        data: {service: "get-plate-foods", params: {user : user, date: day, dish: dish}, auth_key: "p7r1AsDkhK6ejBbUbbzb"}
    })
        .done(function( data ) {

            $('#jar table').html('');
            $('#jar table').append('<tr><th>Názov jedla</th><th>Kalórie [Kj]</th><th>Kalórie [kcal]</th><th>Bielkoviny[g]</th><th>Tuk [g]</th><th>Cukor [g]</th><th>Množstvo [g]</th></tr>');

            $.each(data.result,function(index,value){
                if (data.result[index]!=undefined)
                $('#jar table').append('<tr>'
                    +'<td>'+data.result[index].name+'</td>'
                    +'<td>'+data.result[index].energy_kj +'</td>'
                    +'<td>'+data.result[index].energy_kcal +'</td>'
                    +'<td>'+data.result[index].protein +'</td>'
                    +'<td>'+data.result[index].fat+'</td>'
                    +'<td>'+data.result[index].sugar +'</td>'
                    +'<td><input type="text" id="amount_'+data.result[index].id+'" value="'+data.result[index].amount+'"></td>'
                    +'<td><a href="#" ref="' + data.result[index].id + '" onClick="adjustFood(this, '+data.result[data.result.length-1].user+', '+data.result[data.result.length-1].day+')"><span class="glyphicon glyphicon-plus"></a></td>'
                    +'<td><a href="#" ref="' + data.result[index].id + '" onClick="deleteFood(this, '+data.result[data.result.length-1].user+', '+data.result[data.result.length-1].day+')"><span class="glyphicon glyphicon-remove"></a></td>'+
                    '</tr>');
            });

            renderPlateFoodImages(data.result);
            // zmena pozadia taniera
            changeBackgroundForFluid(data.result);
        });
}

function changeBackgroundForFluid(fluid_all){

    //plate_AVMS
    var img = "plate_";
    $('#plate-div').css('background-image','url(asset/images/plate_.jpg)');
    var acron = ["","","",""];
    if (fluid_all.length>0){
        $.each(fluid_all,function(index,value){
          if (value!=undefined)

            if (value.fluid != undefined && value.fluid == "A") {
               acron[0] ="A";
            }
            if (value.fluid != undefined && value.fluid == "V") {
               acron[1] ="V";
            }
            if (value.fluid != undefined && value.fluid == "M") {
               acron[2] ="M";
            }
            if (value.fluid != undefined && value.fluid == "S") {
               acron[3] ="S";
            }
        });

      $('#plate-div').css('background-image','url(asset/images/'+img+acron.join("")+'.jpg)');
    }

    


}

function sumFood(){
    //var user = getParameterByName('u');
    var user = $.session.get('user');
    //var day = getParameterByName('d');
    var day = $.session.get('day');
    $.ajax({
        type: "POST",
        url: "driver.php",
        dataType: "json",
        data: {service: "get-sum-plate", params: {user : user, date: day},auth_key: "p7r1AsDkhK6ejBbUbbzb"}
    })
        .done(function( data ) {

            $('#sum table').html('');

            if (data.result.daily_nutritions['calories'] - data.result.energy > 0){
                var checkCalories = '<td><font color="green">'+data.result.energy +'</font></td>' +
                    '<td><font color="green">'+data.result.energy_kcal +'</font></td>'

            } else {
                var checkCalories = '<td><font color="red">'+data.result.energy +'</font></td>' +
                    '<td><font color="red">'+data.result.energy_kcal +'</font></td>'
            }
            //$('#sum table').append('<tr><th>Sucet</th><th>Kalorie [Kj]</th><th>Kalorie [kcal]</th><th>Bielkoviny[g]</th><th>Tuk [g]</th><th>Cukor [g]</th><th>Mnozstvo [g]</th></tr>');

            //$('#sum table').append(
            //    '<tr>'
            //    +'<td>'+data.result.name+'</td>'
            //    + checkCalories
            //   +'<td>'+data.result.protein+'</td>'
            //    +'<td>'+data.result.fat +'</td>'
            //    +'<td>'+data.result.sugar+'</td>'
            //    +'<td>'+data.result.amount +'</td>'
            //    +'</tr>'

             //   +'<tr>'
             //   +'<td>Pozadovany pomer</td>'
             //   +'<td>'+data.result.daily_nutritions['calories']+'</td>'
             //   +'<td>'+Math.round(data.result.daily_nutritions['calories'] / 4)+'</td>'
             //   +'<td>'+data.result.daily_nutritions["ratio"]["protein"]+'%</td>'
             //   +'<td>'+data.result.daily_nutritions["ratio"]["fat"]+'%</td>'
             //   +'<td>'+data.result.daily_nutritions["ratio"]["sugar"]+'%</td>'
             //   +'<td>&nbsp;</td>'
             //   +'</tr>'

            $('#sum table').append('<tr>'
        //        +'<td>Aktualny pomer</td>'
        //        +'<td>'+data.result.energy+'</td>'
                +'<td>Nutričný pomer</td>'
                +'<td>Bielkoviny '+data.result.ratio.protein+'%</td>'
                +'<td>Cukry '+data.result.ratio.sugar+'%</td>'
                +'<td>Tuky '+data.result.ratio.fat+'%</td>'
                +'</tr>');

            $('#nutrition table').html('');
            $('#nutrition table').append('<tr>'
                +'<td>Kalórie [Kj | kcal]</td>'+checkCalories
                +'<td>Bielkoviny [g]</td><td>'+data.result.protein+'</td>'
                +'<td>Cukry [g]</td><td>'+data.result.sugar+'</td>'
                +'<td>Tuky [g]</td><td>'+data.result.fat+'</td>'
                +'</tr>');

             $('#fluid table').html('');
             $('#fluid table').append('<tr>'
                +'<td>Alkoholické nápoje [ml]</td><td>'+data.result.fluid['A']+'</td>'
                +'<td>Sladké vody [ml]</td><td>'+data.result.fluid['S']+'</td>'
                +'<td>Čistá voda, minerálka, čaj [ml]</td><td>'+data.result.fluid['V']+'</td>'
                +'<td>Mliečne nápoje [ml]</td><td>'+data.result.fluid['M']+'</td>'
                +'<td>Iné [ml]</td><td>'+data.result.fluid['K']+'</td>'
                +'</tr>');

            // zmena pozadia taniera
           // changeBackgroundForFluid(data.result.fluid);
        });

}

