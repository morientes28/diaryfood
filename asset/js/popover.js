$(document).ready(function() {

  	$("[rel=tip-popup]").popover({
      title : '<div style="text-align:left; color:orange; font-size:16px;"><img src="asset/images/tip.jpg" height="25"/></div>', 
      html: 'true', 
      content : '<div id="popOverBox"></div>' 
	});

  	$("[rel=info-popup]").popover({
      title : '<div style="text-align:left; color:orange; font-size:16px;"><img src="asset/images/info.png" height="25"/></div>', 
      html: 'true', 
      content : '<div id="popOverBox"></div>' 
	});

});