<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once 'vendor/autoload.php';
set_include_path('jedalnicek/generated-classes' . PATH_SEPARATOR . get_include_path());
require_once 'jedalnicek/generated-conf/config.php';

function init($user_id, $day, $dish){
    //$plate = getPlateId($user_id, $day);
    //return getPlateFoods($user_id, $day);
    return getPlateFoods($user_id, $day, $dish);
}

function login($login, $password){
    $user = UserQuery::create()
        ->filterByLogin($login)
        ->filterByPassword($password)
        ->findOne();
    if ($user!=null)
        return array('user'=>$user->getId());
    else
        return false;
}

function getPlateId($user_id, $day, $dish){
    return $plate = PlateQuery::create()
        ->filterByUserId($user_id)
        ->filterByDate($day)
        ->filterByDish($dish)
        ->findOneOrCreate();
}

function getPlateByDay($user_id, $day){
    return $plate = PlateQuery::create()
        ->filterByUserId($user_id)
        ->filterByDate($day)
        ->find();
}

function getByIds($ids, $limit = 3){
    $q = new FoodQuery();
    $q->limit($limit);
    $data = $q->findPKs($ids);
    return (new FoodQuery())->toMyArray($data);
}

function getByName($name, $limit = 3){
    $data = FoodQuery::create()
        ->limit($limit)
        ->filterByName($name.'%')
        ->find();
    return (new FoodQuery())->toMyArray($data);
}

function getByParams($params, $limit = 3){

    $category = (isset($params['category']))? $params['category'] : '';
    $name =     (isset($params['name']))? $params['name'] : '';
    $tags =     (isset($params['tags']))? $params['tags'] : '';
    $order =    (isset($params['order']))? $params['order'] : 'Name';

    $data = FoodQuery::create()
        ->limit($limit)
        ->filterByName($name.'%')
        ->filterByTags($tags.'%')
        ->filterByCategory($category.'%')
        ->orderBy($order,'desc')
        ->find();
    return (new FoodQuery())->toMyArray($data);
}

function getAll($limit = 250){

    $foods = FoodQuery::create()
             ->limit($limit)
             ->find();

    return (new FoodQuery())->toMyArray($foods);

}
function getByCategory($category){

    $foods = FoodQuery::create()
        ->findByCategory($category);

    return (new FoodQuery())->toMyArray($foods);

}
function addFoodToPlate($food_id, $user_id, $day, $dish){

    $q = new FoodQuery();
    $food = $q->findPKs(array($food_id));
    $plate = getPlateId($user_id, $day, $dish);

    $platefood = PlateFoodQuery::create()
                 ->filterByPlate($plate)
                 ->filterByFood($food[0])
                 ->find();

    if (count($platefood)==0){
        $platefood = new PlateFood();
        $platefood->setFood($food[0]);
        $platefood->setPlate($plate);
        $platefood->save();
    } else {
        $amount = $platefood[0]->getAmount() + 100;
        $platefood[0]->setAmount($amount);
        $platefood[0]->postUpdate();
    }

    return (new FoodQuery())->toMyArray($food);

}

function adjustFoodToPlate($food_id, $user_id, $day, $amount, $dish){
    $q = new FoodQuery();
    $food = $q->findOneById($food_id);
    $plate = getPlateId($user_id, $day, $dish);

    $platefood = PlateFoodQuery::create()
        ->filterByPlate($plate)
        ->filterByFood($food)
        ->find();

    $platefood[0]->setAmount($amount);
    $platefood[0]->save();

}

function deleteFoodFromPlate($food_id, $user_id, $day, $dish){

    $plate = getPlateId($user_id, $day, $dish);
    $platefood = PlateFoodQuery::create()
        ->filterByFoodId($food_id)
        ->filterByPlateId($plate->getId())
        ->delete();

    return array('result'=>'ok');
}

function getPlateFoods($user_id, $day, $dish){
    $plate = getPlateId($user_id, $day, $dish);
    $platefoods = PlateFoodQuery::create()
        ->filterByPlateId($plate->getId())
        ->find();

    $data = array();

    foreach ($platefoods as $platefood){
        $food = $platefood->getFood();
        $kf = $platefood->getAmount() / 100;
        $food->setEnergyKj(round($kf * $food->getEnergyKj()));
        $food->setEnergyKcal(round($kf * $food->getEnergyKcal()));
        $food->setFat(round($kf * $food->getFat()));
        $food->setSugar(round($kf * $food->getSugar()));
        $food->setProtein(round($kf * $food->getProtein()));
        $food->setAmount($platefood->getAmount());
        $data[] = $food;
    }

    return (new FoodQuery())->toMyArray($data);
}

function getSumPlate($user_id, $day){
    $plates = getPlateByDay($user_id, $day);
    $platefoods = array();
    foreach ($plates as $plate){
        $tmpfoods = PlateFoodQuery::create()
            ->filterByPlateId($plate->getId());
           // ->findOne();
        if(!empty($tmpfoods))
            $platefoods[] = $tmpfoods;
    }

    $cal = 0;
    $kcal = 0;
    $protein  = 0;
    $fat = 0;
    $sugar  = 0;
    $amount = 0;
    $fluid_all = array('S'=>0, 'A'=>0, 'V'=>0, 'M'=>0, 'K'=>0);

    foreach ($platefoods as $plates){
      foreach ($plates as $platefood){

        $food = $platefood->getFood();

        $kf = $platefood->getAmount() / 100;
        $food->setFat(round($kf * $food->getFat()));
        $food->setSugar(round($kf * $food->getSugar()));
        $food->setProtein(round($kf * $food->getProtein()));
        $food->setEnergyKj(round($kf * $food->getEnergyKj()));
        $food->setEnergyKcal(round($kf * $food->getEnergyKcal()));
        $food->setAmount($platefood->getAmount());

        $cal += $food->getEnergyKj();
        $kcal += $food->getEnergyKcal();
        $protein  += $food->getProtein();
        $fat += $food->getFat();
        $sugar  += $food->getSugar();
        $amount += $platefood->getAmount();
        if ($food->getFluid()!=null && $food->getFluid()!=""){
            $fluid_all[$food->getFluid()] += $platefood->getAmount(); 
        }
      }
    }


    $daily_nutritions = getDailyNutritions($user_id);
    $ratio = getNutritionRatio($sugar, $protein, $fat);


    return array("name"=>"Nutričné hodnoty",
                "energy"=>$cal,
                "energy_kcal"=>$kcal,
                "protein"=>$protein,
                "fat"=>$fat,
                "sugar"=>$sugar,
                "amount"=>$amount,
                "daily_nutritions"=>$daily_nutritions,
                "ratio"=>$ratio,
                "fluid"=>$fluid_all);
}


function getDailyNutritions($user){
    $q = new UserQuery();
    $userObj =  $q->findPK($user);

    switch ($userObj->getProgram()) {
        case 1:
            $ratio = array("protein"=>10, "sugar"=>20, "fat"=>30);
            break;
        case 2:
            $ratio = array("protein"=>10, "sugar"=>20, "fat"=>30);
            break;
        default:
            $ratio = array("protein"=>10, "sugar"=>20, "fat"=>30);

    }
    $result = array("calories"=>$userObj->getBmr(),"ratio"=>$ratio);
    return $result;
}

function getNutritionRatio($sugar, $protein, $fat){
    $distribution = $sugar + $protein + $fat;
    $distribution = ($distribution!=0)?$distribution : 1;
    $p_sugar = $sugar / $distribution * 100;
    $p_protein = $protein / $distribution * 100;
    $p_fat = $fat / $distribution * 100;
    return array("protein"=>round($p_protein), "sugar"=>round($p_sugar), "fat"=>round($p_fat));
}

function getBMRIndex($weight, $age, $height, $gender, $sport, $program, $user){
    /*
     * zeny: BMR = 655 + (9,6 x hmotnosť v kilogramoch) + (1,8 x výška v centimetroch) – (4,7 x vek)
     * muzi: BMR = 660 + (13,7 x hmotnosť v kilogramoch) + (5 x výška v centimetroch) – (6,8 x vek)
     */
    if ($gender=="M"){
        $bmr = (660 + (13.7 * $weight) + (5 * $height) - (6.8 * $age)) * $sport ;
    } else {
        $bmr = (660 + (9.6  * $weight) + (1.8 * $height) - (4.7 * $age)) * $sport;
    }

    $q = new UserQuery();
    $userObj =  $q->findPK($user);
    $userObj->setAge($age);
    $userObj->setHeight($height);
    $userObj->setWeight($weight);
    $userObj->setBmr($bmr);
    $userObj->setSport($sport);
    $userObj->setGender($gender);
    $userObj->setProgram($program);
    $userObj->save();

    return $bmr;

}

function getTemplates($name = '%'){

    /*$plate = PlateQuery::create()
        ->setDistinct('name')
        ->filterByName($name)
        ->filterByName( '', Propel\Runtime\ActiveQuery\Criteria::NOT_EQUAL)
        ->find();*/

    /* $c = new Propel\Runtime\ActiveQuery\Criteria();
     $c->addSelectColumn('name');
     $c->setDistinct();
     $rs = PlatePeer::doSelect($c);*/

    return (new PlateQuery())->getDistinctPlateByName($name);


    //return (new PlateQuery())->toMyArray($plate);
}

function setTemplate($name, $user, $date, $dish){

    $plates = PlateQuery::create()
        ->filterByName($name)
        ->find();

    foreach ($plates as $plate){
        $plateX = new Plate();
        $plateX->setUserId($user);
        $plateX->setDate($date);
        $plateX->setDish($plate->getDish());
        $plateX->save();

        $tmpfoods = PlateFoodQuery::create()
            ->filterByPlateId($plate->getId())
            ->find();

        foreach ($tmpfoods as $tmpfood){
           $newPlateFood = new PlateFood();
           $newPlateFood->setPlate($plateX);
           $newPlateFood->setFood($tmpfood->getFood());
           $newPlateFood->setAmount($tmpfood->getAmount());
           $newPlateFood->save();
        }
    }

    return array(0=>'ok');
}

function addTemplate($day, $dish, $name){
    // user = 1 - administrator
    $plates = getPlateByUserAndDate(1, $day);

    foreach ($plates as $plate){
        $plate->setName($name);
        $plate->save();
    }

    return array(0=>'ok');
}

function getPlateByUserAndDate($user, $day){
    return  PlateQuery::create()
        ->filterByUserId($user)
        ->filterByDate($day)
        ->find();
}

function getStatistics($user){
    return array('123...'=>'TODO', '456...'=>'TODO2');
}

