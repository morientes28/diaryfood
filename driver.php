<?php

include 'mysql-propel.php';
//include 'mysql.php';

if (isset($_SERVER['REQUEST_METHOD']))
    if ($_SERVER['REQUEST_METHOD']=="POST")
        $request = $_POST;
    else {
        $request = $_GET;
    }
else {
    // test case
    $request = array('auth_key' => 'p7r1AsDkhK6ejBbUbbzb',  'service' => 'test-data' );
    return;
}


if (isset($request['auth_key'])){
    if (false == validationAuthKey($request['auth_key'])){
        //throw new Exception('Authorization key is not valid.');
        $data = array("result"=>NULL, "exception"=>"Authorization key is not valid.");
        sendResult($data);
    }
} else {
    //throw new Exception('No found authorization key.');
    $data = array("result"=>NULL, "exception"=>"No found authorization key.");
    sendResult($data);
}

if (isset($request['service'])){
	$params = isset($request['params'])? $request['params'] : NULL;
    $data = doService($request['service'], $params);
} else
	$data = array("result"=>NULL, "exception"=>"NO DEFINE SERVICE");

sendResult($data);

function doService($service, $params){
	$limit = isset($params['limit']) ? $params['limit'] : 10;
	switch ($service) {
      case "init":
        $data = array("result"=>init(intval($params['user']), $params['date'], $params['dish']));
        break;
      case "login":
        parse_str($params['form'], $params);
        $data = array("result"=>login($params['login'], $params['password']));
        break;
	  case "all":
	    $data = array("result"=>getAll(250));
	    break;
      case "add-food-to-plate":
        $data = array("result"=>addFoodToPlate($params['id'], $params['user'], $params['date'], $params['dish']));
        break;
      case "adjust-food":
        $data = array("result"=>adjustFoodToPlate($params['id'], $params['user'], $params['date'], $params['amount'], $params['dish']));
        break;
      case "delete-food":
        $data = array("result"=>deleteFoodFromPlate($params['id'], $params['user'], $params['date'], $params['dish']));
        break;
      case "food-category":
        $data = array("result"=>getByCategory($params['category']));
        break;
      case "get-plate-foods":
        $data = array("result"=>getPlateFoods($params['user'], $params['date'], $params['dish']));
        break;
      case "get-sum-plate":
        $data = array("result"=>getSumPlate($params['user'], $params['date']));
        break;
      case "set-template":
        $data = array("result"=>setTemplate($params['name'], $params['user'], $params['date'], $params['dish']));
        break;
	  case "by-id":
	  	$data = array("result"=>getByIds($params['id'], $limit));
	    break;
	  case "by-name":
	    $data = array("result"=>getByName($params['name'], $limit));
	    break;
	  case "by-params":
	    $data = array("result"=>getByParams($params, $limit));
	    break;
      case "bmr-index":

          parse_str($params['form'], $params);

          $data = array("result" => getBMRIndex($params['weight'],
                                                   $params['age'],
                                                   $params['height'],
                                                   $params['gender'],
                                                   $params['sport'],
                                                   $params['program'],
                                                   $params['user']));

        break;
      case "statistics":
	    $data = array("result"=>getStatistics($params['user']));
	    break;
      case "add-template":
	    $data = array("result"=>addTemplate($params['date'], $params['dish'], $params['name']));
	    break;
      case "get-templates":
	    $data = array("result"=>getTemplates($params['name']));
	    break;
	  case "test-data":
	    $data = array("result" => array("id" =>1, "text"=>"option1"));
	    break;
	  default:
	    $data = array("result"=>NULL, "exception"=>"NO SERVICE FOUND :)");
	}

    return $data;
}


function sendResult($data){
    header('Content-type: text/javascript');
    echo json_encode($data, JSON_PRETTY_PRINT);
    exit;
}

function validationAuthKey($key){
    //TODO: overovanie autorizacneho kluca voci DB alebo listu
    if ($key=="p7r1AsDkhK6ejBbUbbzb"){
        return true;
    } else {
        return false;
    }
}
