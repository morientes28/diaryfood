<?php

class UnitTest extends PHPUnit_Framework_TestCase
{

    //TODO: pred testom nech sa vytvori testovacia databaza, vycistia sa data a spusta sa test na nej

    /*public function testInit(){
        exec('echo "q1w2e3" | sudo -S su | mysqldump jedalnicek -u=root --password > testdb.sql');
    }*/

    public function testPropel()
    {
        // setup the autoloading
        require_once 'vendor/autoload.php';

        set_include_path('jedalnicek/generated-classes' . PATH_SEPARATOR . get_include_path());

        // setup Propel
        require_once 'jedalnicek/generated-conf/config.php.test';

        $user = new User();
        $user->setName('Jane');
        $user->setLogin('Austen');
        $user->setEmail('austen@austen234.sk');
        $user->setPassword('Austen');
        $user->save();

      //  echo $user->toJSON();

        $q = new UserQuery();
        $firstUser = $q->findPK(1);

        $f =  new FoodQuery();
        $firstFood = $f->findPK(320);

        $plate = new Plate();
        $plate->setName("obed");
        $plate->addFood($firstFood);
        $plate->save();

        $this->assertNotEmpty($firstUser);
        $this->assertNotEmpty($plate);

    }
    public function testDriver()
    {

        $_POST['service'] = NULL;
        $_POST['params'] = NULL;

        include_once 'driver.php';

        $data = doService( 'test-data', NULL);
        $this->assertNotEmpty($data);

        $data = doService( 'all', NULL);
        $this->assertNotEmpty($data);

        $data = doService( 'by-id', array('id'=>[12]));
        $this->assertNotEmpty($data);

        $data = doService( 'by-name', array('name'=>'rož'));
        $this->assertGreaterThan(0, count($data));

        $data = doService( 'by-params', array('category'=>'obilniny', 'name'=>'rož','tags'=>NULL,'order'=>'sugar'));
        $this->assertGreaterThan(0, count($data));
    }
}
