<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

function getByIds($ids, $limit = 3){
	$con = connect();
	$test = implode(",", $ids);
	$result = mysqli_query($con,"SELECT * FROM food WHERE id in (".$test.") limit 0, ".$limit);
    $data = [];
    while($row = mysqli_fetch_object($result)) {
        //while($row = mysqli_fetch_array($result)) {
        array_push($data, $row);
        // $data[] = $row;
    }
	disconnect($con);
	return $data;
}

function getByName($name, $limit = 3){
	$con = connect();
	$result = mysqli_query($con,"SELECT * FROM food WHERE name like '%".$name."%' limit 0, ".$limit);
	$data = [];
	while($row = mysqli_fetch_object($result)) {
    //while($row = mysqli_fetch_array($result)) {
	  array_push($data, $row);
      // $data[] = $row;
	}
	disconnect($con);
	return $data;
}

function getByParams($params, $limit = 3){

    $category = (isset($params['category']))? $params['category'] : '';
    $name =     (isset($params['name']))? $params['name'] : '';
    $tags =     (isset($params['tags']))? $params['tags'] : '';
    $order =    (isset($params['order']))? ' order by '.$params['order']." desc" : '';

	$con = connect();
	$result = mysqli_query($con,"SELECT * FROM food WHERE category LIKE '%".$category."%'
	 AND name LIKE '%".$name."%' AND tags LIKE '%".$tags."%' ".$order." limit 0, ".$limit);
	$data = [];
    while($row = mysqli_fetch_object($result)) {
	//while($row = mysqli_fetch_array($result)) {
	  array_push($data, $row);
      $data[] = $row;
	}
	disconnect($con);
	return $data;
}

function getAll($limit = 250){

	$con = connect();
	$result = mysqli_query($con,"SELECT * FROM food limit 0,".$limit);
	$data = [];

    while($row = mysqli_fetch_object($result)) {
	//while($row = mysqli_fetch_array($result)) {
	  array_push($data, $row);
     // $data[] = $row;
	}
	disconnect($con);
	return $data;
}

function connect($setting = array("host"=>"localhost", 
							 	  "user" => "morientes",
							 	  "password" => "q1w2e3",
							 	  "db" => "jedalnicek")){

	$con = mysqli_connect($setting['host'], $setting['user'], $setting['password'], $setting['db']);
    mysqli_set_charset($con, "utf8");
    if (mysqli_connect_errno()) {
	    die('Could not connect: ' . mysql_error());
	}
	return $con;
}

function disconnect($con){
	mysqli_close($con);
}

?>